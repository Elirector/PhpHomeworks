<?php
/**
 * Created by PhpStorm.
 * User: Elirector
 * Date: 06.11.2017
 * Time: 16:56
 */

use PHPUnit\Framework\TestCase;
require_once "../app/LocalMax.php";

class LocalMaxTest extends TestCase
{
    private $localMax;

    public function setUp()
    {
        $this->localMax = new LocalMax();
    }

    public function tearDown()
    {
        $this->localMax = null;
    }

    public function testEmptyArray()
    {

        $this->localMax->setValues([]);
        $this->assertEquals([], $this->localMax->find_maximums());
    }

    public function testOneElement()
    {

        $this->localMax->setValues([4]);
        $this->assertEquals([4], $this->localMax->find_maximums());
    }

    public function testOneMaximum()
    {

        $this->localMax->setValues([1,2,5,3]);
        $this->assertEquals([5], $this->localMax->find_maximums());
    }

    public function testLeftBorder()
    {

        $this->localMax->setValues([4,3,2]);
        $this->assertEquals([4], $this->localMax->find_maximums());
    }

    public function testRightBorder()
    {

        $this->localMax->setValues([-10000,0,1000]);
        $this->assertEquals([1000], $this->localMax->find_maximums());
    }


    /*
     *
     */
    public function testTwoMaximums()
    {

        $this->localMax->setValues([1,2,3,2,4,5,6,2]);
        $this->assertEquals([3,6], $this->localMax->find_maximums());
    }
}
