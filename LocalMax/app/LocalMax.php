<?php
/**
 * Created by PhpStorm.
 * User: Elirector
 * Date: 06.11.2017
 * Time: 16:23
 */


require_once '../../Includes/CliWrapper.php';
use Helpers\CliWrapper;

class LocalMax
{

    private $values = [];



    public function fill_values(): void
    {
        $this->values = [];
        foreach (CliWrapper::GetNextLine() as $input) {
            $this->values[] = trim($input);
        }
    }

    /**
     * @param array $values
     */
    public function setValues(array $values)
    {
        $this->values = $values;
    }

    public function find_maximums(): array
    {
        $maxes = [];
        for ($i = 0; $i < count($this->values); $i++) {
            $prev = $i === 0 ? PHP_INT_MIN : $this->values[$i - 1];
            $next = $i === count($this->values) - 1 ? PHP_INT_MIN : $this->values[$i + 1];
            if ($this->values[$i] > $prev && $this->values[$i] > $next) {
                $maxes[] = $this->values[$i];
            }
        }
        return $maxes;
    }
}/*

$cls = new LocalMax();
do {
    $cls->fill_values();
    $maxes = $cls->find_maximums();

    print_r($maxes);
} while (count($maxes) > 0);*/

