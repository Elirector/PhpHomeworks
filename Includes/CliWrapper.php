<?php
/**
 * Created by PhpStorm.
 * User: Elirector
 * Date: 06.11.2017
 * Time: 14:49
 */

namespace Helpers;


class CliWrapper
{
   public static function GetNextLine()
   {
      while (($line=fgets(STDIN))!=="\n") {

          yield $line;
      }
   }
}