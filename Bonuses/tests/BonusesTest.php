<?php
/**
 * Created by PhpStorm.
 * User: Elirector
 * Date: 10.11.2017
 * Time: 10:41
 */

use PHPUnit\Framework\TestCase;

require_once "../app/Bonuses.php";

class BonusesTest extends TestCase
{
    protected $bonuses = null;

    public function setUp()
    {
        $this->bonuses = new Bonuses();
    }

    public function tearDown()
    {
        unset($this->bonuses);
    }

    /**
     * @dataProvider provideBonusesWithException
     */
    public function testIncorrectInputData($week, $workerNumber, $exceptionType)
    {
        $this->expectException($exceptionType);
        $this->bonuses->calcBonus($week, $workerNumber);
    }

    /**
     * @dataProvider providerBonusesWithResult
     */
    public function testCalcBonus($week, $workerNumber, $result)
    {
        $this->assertEquals($result, $this->bonuses->calcBonus($week, $workerNumber));
    }

    public function providerBonusesWithResult()
    {
        return [
            [[[], [], [], [], [], [], []], 3, [0, 0, 0]], //empty array
            [
                [
                    [
                        [5, 4, 3],
                        [4, 4, 4, 5],
                        [2, 5, 5]
                    ],
                    [
                        [],
                        [5, 4, 4, 5],
                        [5, 5, 5]
                    ],
                    [
                        [5, 4, 3],
                        [5, 5],
                        [5, 5, 5]
                    ],
                    [
                        [],
                        [4, 4, 4, 5],
                        [5, 5, 5]
                    ],
                    [
                        [5, 4, 5],
                        [4, 4, 4, 5],
                        [5, 5, 5]
                    ],
                    [
                        [],
                        [4, 4, 4, 5],
                        [5, 5, 5]
                    ],
                    [
                        [],
                        [5,5,4, 4, 4, 5],
                        [5, 5, 5]
                    ],
                ]
                , 3, [3, 7, 0]
            ],
            [
                [
                    [
                        [5, 4, 3],
                        [4, 4, 4, 5],
                        [2, 5, 5]
                    ],
                    [
                        [],
                        [5, 4, 4, 5],
                        [5, 5, 5]
                    ],
                    [
                        [5, 4, 3],
                        [5, 5],
                        [5, 5, 5]
                    ],
                    [
                        [],
                        [4, 4, 4, 5],
                        [5, 5, 5]
                    ],
                    [
                        [5, 4, 5],
                        [4, 4, 4, 5],
                        [5, 5, 5]
                    ],
                    [
                        [],
                        [4, 4, 4, 5],
                        [5, 5, 5]
                    ],
                    [
                        [],
                        [5,5,4, 4, 4, 2],
                        [5, 5, 5]
                    ],
                ]
                , 2, [3, 0]
            ]

        ];
    }

    public function provideBonusesWithException()
    {
        return [
            [[], 3, "IncorrectInputArrayException"],
            [[[], [], [], [], [], [], []], 1, "OutOfRangeException"],
            [[[], [], [], [], [], [], []], 6, "OutOfRangeException"],
            [[[[0]], [], [], [], [], [], []], 3, "OutOfRangeException"]

        ];
    }
}
