<?php
/**
 * Created by PhpStorm.
 * User: Elirector
 * Date: 06.11.2017
 * Time: 15:11
 */
require_once "IncorrectInputArrayException.php";

class Bonuses
{



    public static function calcBonus(array $week, int $workerNumber): array {
        $bonuses = [];
        for ($w = 0;$w<$workerNumber;$w++) {
            $bonuses[$w] = 0;
        }
        if (count($week)<1) throw new IncorrectInputArrayException("week must contain 1 to 7 workdays");
        if ($workerNumber<2||$workerNumber>5) throw new OutOfRangeException("workerNumber should be between 2 and 5");
        foreach ($week as $weekday) {

            if (!is_array($weekday)) throw new IncorrectInputArrayException("Weekdays must be arrays");
            for($w = 0; $w<$workerNumber; $w++) {
                if (!isset($weekday[$w])||$bonuses[$w]<0) continue;
                $marks = $weekday[$w];
                if (!is_array($marks)||count($marks)==0) continue;
                if (($m=min($marks))<1||max($marks)>5) throw new OutOfRangeException("Marks must be between 1 and 5");
                $bonuses[$w] = min($marks)<3?-1:$bonuses[$w]+1;


            }
        }
        foreach ($bonuses as &$b) {
            if ($b<0) {
                $b = 0;
            }
        }

        return $bonuses;
    }
}

function generate($workers_num)
{
    $week = [];
    for ($i = 0; $i <= 7; $i++)
    {
        $week[$i] = [];
        for ($w=0; $w<$workers_num; $w++)
        {

            //Работал в этот день с вероятностью 75%
            if (mt_rand(1, 4) > 1)
            {

                $positive_day = mt_rand(1,1000)>1;
                $week[$i][$w] = [];
                //звонки..
                while (mt_rand(1, 3) > 1)
                {
                    $week[$i][$w][] = mt_rand($positive_day?3:1, 5);
                }

            }
        }

    }
    return $week;
}



//$week = generate(4);
//print_r($week);




//var_export($week);
