<?php
/**
 * Created by PhpStorm.
 * User: Elirector
 * Date: 06.11.2017
 * Time: 16:56
 */

use PHPUnit\Framework\TestCase;
require_once "../app/Sequence.php";

class SequenceTest extends TestCase
{
    public function testEmptyArray()
    {
      $sq = new Sequence();
      $sq->setVals([]);
      $this->assertEquals(0, $sq->find_sequence());
    }

    public function testOneElement()
    {
        $sq = new Sequence();
        $sq->setVals([42]);
        $this->assertEquals(1, $sq->find_sequence());
    }

    public function testUniqueValues()
    {
        $sq = new Sequence();
        $sq->setVals([5,4,3,6]);
        $this->assertEquals(3, $sq->find_sequence());
    }

    public function testTwoSequences()
    {
        $sq = new Sequence();
        $sq->setVals([4,3,4,3,2,1]);
        $this->assertEquals(4, $sq->find_sequence());
    }

    public function testRepeated()
    {
        $sq = new Sequence();
        $sq->setVals([1,2,3,3,3,4,5]);
        $this->assertEquals(3, $sq->find_sequence());

    }

    public function testGrow()
    {
        $sq = new Sequence();
        $sq->setVals([1,2,3,4,5,6]);
        $this->assertEquals(1, $sq->find_sequence());
    }

    public function testReduce()
    {
        $sq = new Sequence();
        $sq->setVals([7,6,5,4,3,2,1]);
        $this->assertEquals(7, $sq->find_sequence());
    }
}
