<?php
/**
 * Created by PhpStorm.
 * User: Elirector
 * Date: 06.11.2017
 * Time: 16:23
 */


require_once '../../Includes/CliWrapper.php';
use Helpers\CliWrapper;

class Sequence
{

    private $vals = [];



    public function fill_values(): void
    {
        $this->vals = [];
        foreach (CliWrapper::GetNextLine() as $input) {
            $this->vals[] = trim($input);
        }
    }

    /**
     * @param array $vals
     */
    public function setVals(array $vals)
    {
        $this->vals = $vals;
    }

    public function find_sequence(): int
    {

      $max = 0;
      $current_max = 1;
      $prev = PHP_INT_MIN;
      foreach ($this->vals as $v) {
          if ($v>$prev) {

              $current_max = 1;
          }
          else {
              $current_max++;
          }
          if ($current_max>$max) {
              $max = $current_max;

          }
          $prev = $v;
      }
      return $max;
    }
}
/*
$cls = new Sequence();
do {
    $cls->fill_values();
    $max = $cls->find_sequence();

    print_r($max);
} while ($max > 0);*/

